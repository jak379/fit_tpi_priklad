import random
import math

# Inicializace abecedy
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

# Náhodně zvolíme pravděpodobnosti pro jednotlivá písmena tak, aby součet byl roven 1
probabilities = [random.random() for _ in alphabet]
total = sum(probabilities)
probabilities = [p/total for p in probabilities]

# Výpočet množství informace pro každé písmeno
info_content = [-math.log2(p) for p in probabilities]

# Výpočet průměrného množství informace
average_info = sum(p*info for p, info in zip(probabilities, info_content))

# Výpis výsledků
for letter, p, info in zip(alphabet, probabilities, info_content):
    print(f'Písmeno {letter}: pravděpodobnost {p:.3f}, množství informace {info:.3f} bits')

print(f'Průměrné množství informace: {average_info:.3f} bits')



# Python3 program for Shannon Fano Algorithm, source: https://www.geeksforgeeks.org/shannon-fano-algorithm-for-data-compression/, https://wikisofia.cz/wiki/Shannon-Fanovo_k%C3%B3dov%C3%A1n%C3%AD
# declare structure node
class  node :
    def __init__(self) -> None:
        # for storing symbol
        self.sym=''
        # for storing probability or frequency
        self.pro=0.0
        self.arr=[0]*20
        self.top=0

# function to find shannon code
def shannon(l, h, p):
    pack1 = 0; pack2 = 0; diff1 = 0; diff2 = 0
    if ((l + 1) == h or l == h or l > h) :
        if (l == h or l > h):
            return
        p[h].top+=1
        p[h].arr[(p[h].top)] = 0
        p[l].top+=1
        p[l].arr[(p[l].top)] = 1
        
        return
    
    else :
        for i in range(l,h):
            pack1 = pack1 + p[i].pro
        pack2 = pack2 + p[h].pro
        diff1 = pack1 - pack2
        if (diff1 < 0):
            diff1 = diff1 * -1
        j = 2
        while (j != h - l + 1) :
            k = h - j
            pack1 = pack2 = 0
            for i in range(l, k+1):
                pack1 = pack1 + p[i].pro
            for i in range(h,k,-1):
                pack2 = pack2 + p[i].pro
            diff2 = pack1 - pack2
            if (diff2 < 0):
                diff2 = diff2 * -1
            if (diff2 >= diff1):
                break
            diff1 = diff2
            j+=1
        
        k+=1
        for i in range(l,k+1):
            p[i].top+=1
            p[i].arr[(p[i].top)] = 1
            
        for i in range(k + 1,h+1):
            p[i].top+=1
            p[i].arr[(p[i].top)] = 0
            

        # Invoke shannon function
        shannon(l, k, p)
        shannon(k + 1, h, p)

# Function to sort the symbols
# based on their probability or frequency
def sortByProbability(n, p):
    temp=node()
    for j in range(1,n) :
        for i in range(n - 1) :
            if ((p[i].pro) > (p[i + 1].pro)) :
                temp.pro = p[i].pro
                temp.sym = p[i].sym

                p[i].pro = p[i + 1].pro
                p[i].sym = p[i + 1].sym

                p[i + 1].pro = temp.pro
                p[i + 1].sym = temp.sym

# function to display shannon codes
def display(n, p):
    print("\n\n\n\tSymbol\tProbability\tCode",end='')
    for i in range(n - 1,-1,-1):
        print("\n\t", p[i].sym, "\t\t", p[i].pro,"\t",end='')
        for j in range(p[i].top+1):
            print(p[i].arr[j],end='')



# Deklaruji strukturu node
p = [node() for i in range(20)]
n = len(alphabet)
for i in range(n):
    p[i].sym = alphabet[i]
    p[i].pro = probabilities[i]

# 1) Seřadím symboly podle pravděpodobnosti nebo frekvence
sortByProbability(n, p)
for i in range(n): # Inicializujeme top hodnoty
    p[i].top = -1
shannon(0, n - 1, p) # Najdeme Shannon-Fanův kód
display(n, p)
print()

# Spočítáme průměrnou délku kódového slova a efektivnost kódu
average_length_shannon_fano = sum(p[i].pro * (p[i].top + 1) for i in range(n))
efficiency_shannon_fano = average_info / average_length_shannon_fano
print(f"Průměrná délka kódového slova (Shannon-Fano): {average_length_shannon_fano:.3f}")
print(f"Efektivnost kódu (Shannon-Fano): {efficiency_shannon_fano:.3f}")

# Vypíšeme binární strom ve formátu Graphviz (podle nul a jedniček v kódech)
def binary_tree(p):
    print("graph G {")
    print("root -- 0;")
    print("root -- 1;")
    codes = []
    for i in range(n):
        code = ''.join(str(p[i].arr[j]) for j in range(p[i].top + 1))
        print(f"{code} -- {p[i].sym};")
        codes.append(code)
    toAdd = {}
    for code in codes:
        code_copy = code
        while code_copy.__len__() > 1:
            toAdd[f"{code_copy[:-1]} -- {code_copy};"] = 1
            code_copy = code_copy[:-1]
    for key in toAdd.keys().__reversed__():
        print(key)

    print("}")
binary_tree(p)

# Z výsledného kódu vytvořte kód zabezpečený. Použijte zabezpečení pomocí sudé parity.
# Zabezpečený kód vytvoříme tak, že přidáme na konec každého kódového slova bit sudé parity

# Funkce pro výpočet sudé parity
def even_parity(bits):
    return sum(bits) % 2

# Vytvořím zabezpečený kód
secure_code = ''
for i in range(n):
    secure_code += p[i].sym
    for j in range(p[i].top + 1):
        secure_code += str(p[i].arr[j]) # Přidám kódové slovo
    secure_code += str(even_parity(p[i].arr)) + ' ' # Přidám bit sudé parity

print(f"Zabezpečený kód: {secure_code}")