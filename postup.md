# Výpočet množství informace pro každý znak

Nejprve jsem vytvořil seznam deseti zdrojových znaků a přiřadil jsem jim náhodné pravděpodobnosti výskytu tak, aby součet všech pravděpodobností byl roven jedné.

Poté jsem pro každý znak vypočítal množství informace pomocí vzorce `I = -log2(p)`, kde `p` je pravděpodobnost výskytu daného znaku.

```
Písmeno a: pravděpodobnost 0.158, množství informace 2.658 bits
Písmeno b: pravděpodobnost 0.019, množství informace 5.716 bits
Písmeno c: pravděpodobnost 0.115, množství informace 3.118 bits
Písmeno d: pravděpodobnost 0.023, množství informace 5.416 bits
Písmeno e: pravděpodobnost 0.182, množství informace 2.456 bits
Písmeno f: pravděpodobnost 0.172, množství informace 2.543 bits
Písmeno g: pravděpodobnost 0.119, množství informace 3.072 bits
Písmeno h: pravděpodobnost 0.021, množství informace 5.570 bits
Písmeno i: pravděpodobnost 0.076, množství informace 3.725 bits
Písmeno j: pravděpodobnost 0.114, množství informace 3.129 bits
```

# Sestrojení efektivního binárního kódu

Pro sestrojení efektivního binárního kódu jsem použil metodu [Shannon-Fanovým kódováním](https://wikisofia.cz/wiki/Shannon-Fanovo_k%C3%B3dov%C3%A1n%C3%AD). Nejprve jsem znaky seřadil podle pravděpodobnosti výskytu od nejvyšší po nejnižší. Poté jsem postupně rozděloval seznam znaků na dvě části tak, aby součet pravděpodobností v obou částech byl co nejvíce vyrovnaný. První části jsem přiřadil binární kód začínající nulou a druhé části kód začínající jedničkou. Tento postup jsem opakoval rekurzivně pro obě části, dokud nezbyl jen jeden znak.
Průměrnou délku kódového slova jsem vypočítal jako vážený průměr délky kódových slov, kde váhou byla pravděpodobnost výskytu daného znaku a efektivnost kódu jsem vypočítal jako poměr průměrného množství informace a průměrné délky kódového slova.

| Znak | Pravděpodobnost    | Kód  |
|------|--------------------|------|
|e     |0.18226877770072616 |00    |
|f     |0.17162706290554172 |010   |
|a     |0.15848983486857887 |011   |
|g     |0.11890149817815833 |100   |
|c     |0.11521657671331113 |101   |
|j     |0.11434042414986594 |110   |
|i     |0.07565065371596269 |1110  |
|d     |0.023421056886990144|11110 |
|h     |0.0210531550479425  |111110|
|b     |0.019030959832922443|111111|

# Binární strom

![binarni_strom.jpg](binarni_strom.jpg)

```
graph G {
root -- 0;
root -- 1;
111111 -- b;
111110 -- h;
11110 -- d;
1110 -- i;
110 -- j;
101 -- c;
100 -- g;
011 -- a;
010 -- f;
00 -- e;
0 -- 00;
01 -- 010;
0 -- 01;
01 -- 011;
10 -- 100;
1 -- 10;
10 -- 101;
11 -- 110;
111 -- 1110;
1111 -- 11110;
11111 -- 111110;
1 -- 11;
11 -- 111;
111 -- 1111;
1111 -- 11111;
11111 -- 111111;
}
```

# Vytvoření zabezpečeného kódu

Přidáním paritního bitu jsem vytvořil zabezpečený kód. Sečetl jedničky v binárním kódu a pokud byl jejich součet lichý, přidal jsem jedničku, jinak nulu.
```
Zabezpečený kód: b1111110 h1111101 d111100 i11101 j1100 c1010 g1001 a0110 f0101 e000
```

# Zdrojový kód

- [kodovani.py](kodovani.py)